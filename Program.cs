﻿using System;
using System.Diagnostics;
using System.IO;
using Microsoft.Win32;

namespace qsiReg
{
    public class Functions
    {
        public static int DisplayHelp()
        {
            Console.WriteLine("About:");
            Console.WriteLine("------------");
            Console.WriteLine("qsiReg.exe scans the local machine and determines whether a unit is vulnerable (Per Rule 11) to Logical Attacks at the Dispenser level. " +
                              "It will automatically enact the proper protections where possible, and will output the previous/current status of the machine as to provide actionable data.");
            Console.WriteLine("");
            Console.WriteLine("How to use:");
            Console.WriteLine("------------");
            Console.WriteLine("Run qsiReg.exe directly through a remote console, or by utilizing NCR's CopyFile via the provided install.bat.");
            Console.WriteLine("After completion, a log file will be created in the \\install\\logs directory, named QSIdplReport.log");
            Console.WriteLine("");
            Console.WriteLine("How to interpret the log:");
            Console.WriteLine("------------");
            Console.WriteLine("The 'Quick Status:' will display 'Pass' only if the dispenser and XFS version pass all required tests for successful protection against Logical attacks, as well as the Software stack being 'qsiReg Applicable'. " +
                              "All other scenarios will result in a Fail.");
            Console.WriteLine("");
            Console.WriteLine("  - If the 'Quick Status' states 'Fail', check 'qsiReg Applicable, 'XFS Pass' and 'DPL Pass' for 'False' to discover which is the issue.");
            Console.WriteLine("  - Generally, if 'XFS Pass' fails the device is not using at least Edge 5 or is using a different CDM component altogether.");
            Console.WriteLine("  - If the 'DPL Pass' results in a 'Fail, then the issue is likely that the Dispenser does not support L3 authentication and a PDEE is needed.");
            Console.WriteLine("  - If the 'qsiReg Applicable' states 'Fail, then the qsiReg program has not yet been configured for this machine type. Please consult QSI to determine if this is intentional or if the application can be updated.");
            Console.WriteLine("  - Please pass along any discoveries to QSI prior to implementing any fixes so that QSI may help to direct/verify/debug further and prevent any misuse/misunderstandings.");

            return 0;
        }
    }

    public class MainClass
    {
        static int Main(string[] args)
        {
            Console.WriteLine("Logic based on Rule 11 from the NCR Whitepaper on Logical Attacks - ");
            Console.WriteLine("This application comes without warranty and QSI, INC does not accept any responsibility for misuse, misapplication or incorrect information provided on behalf of the manufacturer.");
            Console.WriteLine("");
            Console.WriteLine("NCR White Paper - ");
            Console.WriteLine("https://www.ncr.com/sites/default/files/brochures/17fin5025_a_sec_rqts_protect_logical_attacks_wp.pdf");
            Console.WriteLine("");
            if (args.Length == 0)
            {
                string dispType = "Unknown";
                int dispConfReg = 3;
                int dispPLReg = 3;
                int sprayAuthSupported = 3;
                int swMajorVersionint = 0;
                string machineType = "Unknown";
                string swStack = "Unknown";
                string swVersion = "Unknown";
                string componentType = "USBCurrencyDispenser";
                string xfsVersion = "N/A";
                string xfsPatch = "03.01.00";
                string[] subKeys = null;
                string xfsPass = "Unknown";
                string changesAllowed = "N/A";
                string dplPrevious = "Unknown";
                string dplCurrent = "Unknown";
                string dispConf = "N/A";
                string dplPass = "Unknown";
                string quickStatus = "Fail";
                string itmFolder = @"C:\Program Files\uGenius\";
                string targetMachine = "False";
                var swMajorVersionstr = "Unknown";
                bool dplExists = true;
                
                // Per NCR - The important item is USB Currency Dispenser 03.01.00.  
                // In the USA XFS 06.03.00 is not used. 
                // Edge 5 uses XFS 06.01.10 which utilizes USB Currency Dispenser 03.01.00 component. So all Edge 5 software provides the protection called out in Rule 11.
                /* "https://www.ncr.com/sites/default/files/brochures/17fin5025_a_sec_rqts_protect_logical_attacks_wp.pdf" */

                RegistryKey hwConfKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\NCR\APTRA Self-Service Support (NCR Features)\Properties\HW_DEVICE", false);

                if (hwConfKey != null)
                {
                    foreach (string sub in hwConfKey.GetSubKeyNames())
                    {
                        if (sub.Contains("CASH-01-"))
                        {
                            dispType = (string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\NCR\APTRA Self-Service Support (NCR Features)\Properties\HW_DEVICE\" + sub, "Feature_Name", "Unknown");
                            break;
                        }
                    }
                }
                else
                {
                    Debug.WriteLine("Application: hwConfKey is null. Fatal Error.");
                    Console.WriteLine("Fatal Error with reading HW_DEVICE Subkey. Application will terminate.");
                    return -1;
                }

                if (Directory.Exists(itmFolder))
                {
                    machineType = "ITM";
                    swStack = "Aptra Activate";
                    try
                    {
                        swVersion = (string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\NCR\VERSIONS", "ICUSN-MUP", null);
                    }
                    catch
                    {
                    }

                    if (swVersion == null)
                    {
                        swVersion = "Unknown";
                    }
                }
                else
                {
                    try
                    {
                        swVersion = (string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\NCR\VERSIONS", "AANDC-CD2-MUP", null);
                    }
                    catch
                    {
                    }

                    if (swVersion != null)
                    {
                        machineType = "ATM";
                        swStack = "AANDC";
                    }
                    else
                    {
                        try
                        {
                            swVersion = (string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\NCR\VERSIONS", "StandardBase-CD2-MUP", null);
                        }
                        catch
                        {
                        }

                        if (swVersion != null)
                        {
                            machineType = "ATM";
                            swStack = "Aptra Edge";
                            targetMachine = "True";
                        }
                        else
                        {
                            machineType = "Unknown";
                            swStack = "Unknown";
                            swMajorVersionstr = "0";
                        }
                    }
                }

                // attempt cast string major version to int so we can compare it
                swMajorVersionstr = swVersion.Substring(0, 2);

                if (Int32.TryParse(swMajorVersionstr, out swMajorVersionint))
                {
                    Debug.WriteLine("Correctly cast major version string to int");
                }
                else
                {
                    swMajorVersionint = 0;
                }

                //New XFS Check per Rule 11
                try
                {
                    RegistryKey xfsCDMSubKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\NCR\APTRA\Aggregate Installer\Inventory\Component\" + componentType, false);
                    subKeys = xfsCDMSubKey.GetSubKeyNames();
                }
                catch
                {
                    Debug.WriteLine("Error attempting to check xfsCDMSubKey");
                    xfsVersion = "Unknown";
                }

                if (xfsVersion != "Unknown" && subKeys != null && subKeys.Length != 0)
                {
                    xfsVersion = (string)subKeys.GetValue(0);

                    var v1 = new Version(xfsVersion);
                    var v2 = new Version(xfsPatch);

                    if (xfsPatch.CompareTo(xfsVersion) <= 0 && swMajorVersionint >= 5)
                    {
                        Debug.WriteLine("XFS Check: This XFS version reaches or surpasses the requirements for successful implementation of L3 Authentication.");
                        xfsPass = "True";
                    }
                    else
                    {
                        Debug.WriteLine("XFS Check: This XFS version is vulnerable to logical attacks and must be upgraded.");
                        xfsPass = "False";
                    }
                }
                else
                {
                    Debug.WriteLine("XFS Check: Could not verify component version.");
                    xfsPass = "Unknown";
                }

                //Make sure the disp is configured
                try
                {
                    dispConfReg = (int)Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\NCR\APTRA Self-Service Support (NCR Features)\" + dispType + @"\Operational Parameters", "Device Configured", 4);
                }
                catch
                {
                    Debug.WriteLine("Device Config Check: Registry value for Device Configured can not be read.");
                }

                //Check DPL
                try
                {
                    if (dispType != "RS232SprayDispenser")
                    {
                        dispPLReg = (int)Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\NCR\APTRA Self-Service Support (NCR Features)\" + dispType + @"\Operational Parameters", "Dispense Protection Level", 4);
                    }
                    else
                    {
                        //NCR chose to have one dispenser use *Dispenser* reg key rather than *Dispense*, go figure. I'm looking at you Spray Dispenser.
                        dispPLReg = (int)Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\NCR\APTRA Self-Service Support (NCR Features)\" + dispType + @"\Operational Parameters", "Dispenser Protection Level", 4);
                    }
                }
                catch
                {
                    Debug.WriteLine("DPL Check: Registry value for DPL can not be read. Fatal Error.");
                    dplPrevious = "Unknown";
                    dplExists = false;
                }

                if (dplExists == true)
                {
                    switch (dispPLReg)
                    {
                        case 0:
                            Debug.WriteLine("DPL Check: Dispenser is set to USB Authentication and needs to be updated.");
                            dplPrevious = "USB";
                            break;
                        case 1:
                            Debug.WriteLine("DPL Check: Dispenser is set to Logical Authentication and needs to be updated.");
                            dplPrevious = "Logical";
                            break;
                        case 2:
                            Debug.WriteLine("DPL Check: Dispenser is properly configured for Physical Authentication.");
                            dplPrevious = "Physical";
                            break;
                        case 3:
                            Debug.WriteLine("DPL Check: Variable for DPL was never changed. Fatal Error.");
                            dplPrevious = "Unknown";
                            break;
                        case 4:
                            Debug.WriteLine("DPL Check: Variable for DPL is set to the default value, key read unsuccessfully. Fatal Error.");
                            dplPrevious = "Unknown";
                            break;
                    }
                }

                //Spray and SDC dispensers may not be able to be upgraded, check config to know
                if (dispType == "RS232SprayDispenser" || dispType == "SdcDispenser")
                {
                    try
                    {
                        sprayAuthSupported = (int)Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\NCR\APTRA Self-Service Support (NCR Features)\" + dispType + @"\Operational Parameters", "AuthenticationSupported", 4);
                    }
                    catch
                    {
                        Debug.WriteLine("SDC/RS232 Only: Registry value for Authentication Supported can not be read.");
                        changesAllowed = "Unknown";
                    }

                    switch (sprayAuthSupported)
                    {
                        case 0:
                            Debug.WriteLine("SDC/RS232 Only: The dispenser does not support DPL3. Machine needs to be updated to support Physical Authentication.");
                            changesAllowed = "False";
                            break;
                        case 1:
                            Debug.WriteLine("SDC/RS232 Only: The dispenser is able to support DPL3.");
                            changesAllowed = "True";
                            break;
                        case 3:
                            Debug.WriteLine("SDC/RS232 Only: Variable for DPL was never changed. Fatal Error.");
                            changesAllowed = "Unknown";
                            break;
                        case 4:
                            Debug.WriteLine("SDC/RS232 Only: Variable for DPL is set to the default value, key read unsuccessfully. Fatal Error.");
                            changesAllowed = "Unknown";
                            break;
                    }
                }

                if (dplPrevious != "Physical" && (changesAllowed == "True" || changesAllowed == "N/A"))
                {
                    Debug.WriteLine("DPL Set: Attempting to set DPL to Physical Authentication");
                    try
                    {
                        Registry.SetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\NCR\APTRA Self-Service Support (NCR Features)\" + dispType + @"\Operational Parameters", "Dispense Protection Level", 2);
                    }
                    catch
                    {
                        Debug.WriteLine("DPL Set: Failed to set DPL to Physical Authentication. Fatal Error.");
                        dplCurrent = dplPrevious;
                    }

                    if ((int)Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\NCR\APTRA Self-Service Support (NCR Features)\" + dispType + @"\Operational Parameters", "Dispense Protection Level", 4) == 2)
                    {
                        Debug.WriteLine("DPL Set: Successfully set DPL to Physical Auth. Success.");
                        dplCurrent = "Physical";
                    }
                    else
                    {
                        Debug.WriteLine("DPL Set: DPL is not set to Physical Authentication as expected. Fatal Error.");
                        dplCurrent = dplPrevious;
                    }
                }
                else
                {
                    dplCurrent = dplPrevious;
                }

                // Logic for disp configured check
                switch (dispConfReg)
                {
                    case 0:
                        Debug.WriteLine("Device Config Check: Expected Dispenser is not configured.");
                        dispConf = "False";
                        break;
                    case 1:
                        Debug.WriteLine("Device Config Check: Expected Dispenser is properly configured.");
                        dispConf = "True";
                        break;
                    case 3:
                        Debug.WriteLine("Device Config Check: Variable for dispConfReg was never changed.");
                        dispConf = "Unknown";
                        break;
                    case 4:
                        if (dispType != "USBMediaDispenser")
                        {
                            Debug.WriteLine("Device Config Check: Variable for dispConfReg is set to the default value, key read unsuccessfully.");
                            dispConf = "Unknown";
                            break;
                        }
                        else
                        {
                            Debug.WriteLine("Device Config Check: Variable for dispConfReg does not exist on USBMediaDispenser. Exception to rule, continuing.");
                            break;
                        }
                }

                if (dispConf != "False" && dplCurrent == "Physical")
                {
                    dplPass = "True";
                }
                else
                {
                    dplPass = "False";
                }

                if (dplPass == "True" && xfsPass == "True" && targetMachine == "True")
                {
                    quickStatus = "Pass";
                }
                
                Console.WriteLine("========================================");
                Console.WriteLine("  qsiReg Results - " + DateTime.Now);
                Console.WriteLine("========================================");
                Console.WriteLine("Quick Status: " + quickStatus);  //Passed-Failed
                Console.WriteLine("------");
                Console.WriteLine("");
            
                Console.WriteLine("Machine Information");
                Console.WriteLine("---------------------------------");
                Console.WriteLine("Machine Type: " + machineType);
                Console.WriteLine("NCR Software: " + swStack);
                Console.WriteLine("Software Version: " + swVersion);
                Console.WriteLine("------");
                Console.WriteLine("qsiReg Applicable: " + targetMachine);
                Console.WriteLine("------");
                Console.WriteLine("");
                

                Console.WriteLine("Dispenser Settings");
                Console.WriteLine("---------------------------------");
                Console.WriteLine("Disp. Type: " + dispType);
                Console.WriteLine("Disp. Configured: " + dispConf);
                Console.WriteLine("DPL Changes Allowed: " + changesAllowed);
                Console.WriteLine("DPL Previous: " + dplPrevious);
                Console.WriteLine("DPL Current: " + dplCurrent);
                Console.WriteLine("DPL Requirement: Physical");
                Console.WriteLine("------");
                Console.WriteLine("DPL Pass: " + dplPass);
                Console.WriteLine("------");
                Console.WriteLine("");

                Console.WriteLine("XFS CDM Settings");
                Console.WriteLine("---------------------------------");
                Console.WriteLine("Disp. Component Type: Feature TBD, Only working on SelfServ");
                Console.WriteLine("Disp. Component Ver.: " + xfsVersion);
                Console.WriteLine("Disp. Component Req.: " + xfsPatch);
                Console.WriteLine("------");
                Console.WriteLine("XFS Pass: " + xfsPass);
                Console.WriteLine("------");
                Console.WriteLine("");

                return 0;
            }

            if (args.Length == 1 && HelpRequired(args[0]))
            {
                Functions.DisplayHelp();
                return 0;
            }
            else
            {
                Functions.DisplayHelp();
                return 0;
            }
        }

        public static bool HelpRequired(string param)
        {
            // Accept various help parameters
            return param == "-h" || param == "--help" || param == "/?" || param == "help" || param == "/h";
        }
    }
}
